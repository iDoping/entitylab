﻿using Microsoft.EntityFrameworkCore;
using WorkWithDB.Models;

namespace WorkWithDB.Data
{
    public class StudentsContext:DbContext
    {
        public StudentsContext(DbContextOptions<StudentsContext> options) : base(options)
        {
        }

        public DbSet<Students> Students { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Students>().ToTable("Students");
        }
    }
}
