﻿using System;
using System.Linq;
using WorkWithDB.Models;

namespace WorkWithDB.Data
{
    public class DataInit
    {
        public static void Initialize(StudentsContext context)
        {
            context.Database.EnsureCreated();

            if (context.Students.Any())
            {
                return;
            }

            var students = new Students[]
            {
            new Students{Name="Carson",Surname="Alexander",Birthday=DateTime.Parse("2005-09-01"),Adress="SomeAdr"},
            new Students{Name="Meredith",Surname="Alonso",Birthday=DateTime.Parse("2002-09-01"),Adress="SomeAdr"},
            new Students{Name="Arturo",Surname="Anand",Birthday=DateTime.Parse("2003-09-01"),Adress="SomeAdr"},
            new Students{Name="Gytis",Surname="Barzdukas",Birthday=DateTime.Parse("2002-09-01"),Adress="SomeAdr"},
            new Students{Name="Yan",Surname="Li",Birthday=DateTime.Parse("2002-09-01"),Adress="SomeAdr"},
            new Students{Name="Peggy",Surname="Justice",Birthday=DateTime.Parse("2001-09-01"),Adress="SomeAdr"},
            new Students{Name="Laura",Surname="Norman",Birthday=DateTime.Parse("2003-09-01"),Adress="SomeAdr"},
            new Students{Name="Nino",Surname="Olivetto",Birthday=DateTime.Parse("2005-09-01"),Adress="SomeAdr"}
            };

            foreach (Students s in students)
            {
                context.Students.Add(s);
            }
            context.SaveChanges();
        }
    }
}
